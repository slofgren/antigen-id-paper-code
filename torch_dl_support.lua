function permute(tab, n, count)
  n = n or #tab
  for i = 1, count or n do
    local j = math.random(i, n)
    tab[i], tab[j] = tab[j], tab[i]
  end
  return tab
end


function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end


function pearson(x, y)
  x = x - x:mean()
  y = y - y:mean()
  return x:dot(y) / (x:norm() * y:norm())
end

function random_seq(length)
    local rand_seq = {}
    for i = 1,length do
        rand_seq[i] = i
        end
    rand_seq = torch.Tensor(permute(rand_seq,length))
    return rand_seq
end

function torch_process(dataset,resp_col,cuda
,feature_end
        ,train_quant,val_quant,test_quant)
    local rand_seq = random_seq(#dataset)
    local train_len = math.ceil(#dataset * train_quant)
    local val_len = math.ceil(#dataset * val_quant)
    local test_len = #dataset -train_len - val_len
    local train_seq = rand_seq[{{1,train_len}}]
    local val_seq = rand_seq[{{train_len + 1,train_len + val_len}}]
    local test_seq = rand_seq[{{train_len + val_len + 1,#dataset}}]
    local indices = {train_seq = train_seq
    ,val_seq = val_seq
    ,test_seq = test_seq}
    local train_feat = torch.Tensor(train_len,feature_end)
    local val_feat = torch.Tensor(val_len,feature_end)
    local test_feat = torch.Tensor(test_len,feature_end)
    local train_resp = {}
    local val_resp = {}
    local test_resp = {}
    --Create the trainset
    for i = 1,train_len do
        temp = torch.Tensor(dataset[train_seq[i]])
        train_feat[i] = temp[{{1,feature_end}}]
        train_resp[i] = temp[{{resp_col}}]
        end
    local trainset = {}
    trainset.data = train_feat
    trainset.response = train_resp
    function trainset:size() return train_len end
    setmetatable(trainset,
    {__index = function(t, i)
            return{t.data[i],t.response[i]}
        end}
    )
    --Create the valset
    for i = 1,val_len do
        temp = torch.Tensor(dataset[val_seq[i]])
        val_feat[i] = temp[{{1,feature_end}}]
        val_resp[i] = temp[{{resp_col}}]
        end
    local valset = {}
    valset.data = val_feat
    valset.response = val_resp
    function valset:size() return val_len end
    setmetatable(valset,
    {__index = function(t, i)
            return{t.data[i],t.response[i]}
        end}
    )
    --Create the testset
    for i = 1,test_len do
        temp = torch.Tensor(dataset[test_seq[i]])
        test_feat[i] = temp[{{1,feature_end}}]
        test_resp[i] = temp[{{resp_col}}]
        end
    local testset = {}
    testset.data = test_feat
    testset.response = test_resp
    function testset:size() return test_len end
    setmetatable(testset,
    {__index = function(t, i)
            return{t.data[i],t.response[i]}
        end}
    )
    --Finally, Convert to Cuda, if requested
    if cuda == "True" then
	print("Cuda is True")
        trainset.data = trainset.data:cuda()
        for i = 1,train_len do
            trainset.response[i] = trainset.response[i]:cuda()
            end
        valset.data = valset.data:cuda()
        for i = 1,val_len do
            valset.response[i] = valset.response[i]:cuda()
            end
        testset.data = testset.data:cuda()
        for i = 1,test_len do
            testset.response[i] = testset.response[i]:cuda()
            end
        end
    return trainset,valset,testset,indices
end


function predict_model(model, dataset)
    preds = torch.Tensor(dataset:size())
    for i=1,dataset:size() do
      preds[{{i}}] = model:forward(dataset.data[i])
      end
    preds_eval = torch.Tensor(dataset:size())
    for i = 1,(#preds_eval)[1] do
        preds_eval[i] = dataset[i][2][1]
        end
    out = {}
    out.predicted = preds
    out.true_val = preds_eval
    return out
end

function export_results(scale_set,indices,path,torch_model)

    local train_out = torch.Tensor(scale_set.datasets[1]:size(),2)
    train_out[{{},{1}}] = indices.train_seq
    --train_out[{{},{2}}] = torch_model:forward(scale_set.datasets[1].data)
    for i=1,scale_set.datasets[1]:size() do
      train_out[{{i},{2}}] = torch_model:forward(scale_set.datasets[1].data[i])
      end
    train_out[{{},{2}}] = train_out[{{},{2}}]:add(scale_set.response_scalings[1])
    train_out[{{},{2}}] = train_out[{{},{2}}]:mul(scale_set.response_scalings[2])
    train_out = torch.totable(train_out)

    local val_out = torch.Tensor(scale_set.datasets[2]:size(),2)
    val_out[{{},{1}}] = indices.val_seq
    --val_out[{{},{2}}] = torch_model:forward(scale_set.datasets[2].data)
    for i=1,scale_set.datasets[2]:size() do
      val_out[{{i},{2}}] = torch_model:forward(scale_set.datasets[2].data[i])
      end
    val_out[{{},{2}}] = val_out[{{},{2}}]:add(scale_set.response_scalings[1])
    val_out[{{},{2}}] = val_out[{{},{2}}]:mul(scale_set.response_scalings[2])
    val_out = torch.totable(val_out)

    local test_out = torch.Tensor(scale_set.datasets[3]:size(),2)
    test_out[{{},{1}}] = indices.test_seq
    --test_out[{{},{2}}] = torch_model:forward(scale_set.datasets[3].data)
    for i=1,scale_set.datasets[3]:size() do
      test_out[{{i},{2}}] = torch_model:forward(scale_set.datasets[3].data[i])
      end
    test_out[{{},{2}}] = test_out[{{},{2}}]:add(scale_set.response_scalings[1])
    test_out[{{},{2}}] = test_out[{{},{2}}]:mul(scale_set.response_scalings[2])
    test_out = torch.totable(test_out)

    --Now write
    csvigo.save{path = path .. "/" .. tcr_name .. "_" .. data_type .. "_trainpreds.csv"
        ,data = train_out,separator = ",",mode = "raw"}
    csvigo.save{path = path .. "/" .. tcr_name .. "_" .. data_type .. "_valpreds.csv"
        ,data = val_out,separator = ",",mode = "raw"}
    csvigo.save{path = path .. "/" .. tcr_name .. "_" .. data_type .. "_testpreds.csv"
        ,data = test_out,separator = ",",mode = "raw"}
    return
end

function scale_data(trainset,valset,testset,feature_scale,response_scale)
    local train_means = {}
    local train_sds = {}

    local train_scale = trainset
    local val_scale = valset
    local test_scale = testset

    out = {}
    if feature_scale == "Yes" then
        for i = 1,feature_end do
            train_means[i] = train_scale.data[{{},{i}}]:mean()
            train_sds[i] = train_scale.data[{{},{i}}]:std()
            train_scale.data[{{},{i}}]:add(-train_means[i])
            train_scale.data[{{},{i}}]:div(train_sds[i])
            val_scale.data[{{},{i}}]:add(-train_means[i])
            val_scale.data[{{},{i}}]:div(train_sds[i])
            test_scale.data[{{},{i}}]:add(-train_means[i])
            test_scale.data[{{},{i}}]:div(train_sds[i])
            end
        out.feature_scalings = {train_means,train_sds}
        end

    if response_scale == "Yes" then
        local temp_resp = torch.Tensor(train_scale:size())
        for i=1,train_scale:size() do
            temp_resp[i] = train_scale.response[i][1]
            end
        local train_resp_mean = temp_resp:mean()
        local train_resp_std = temp_resp:std()

        for i=1,train_scale:size() do
            train_scale.response[i]:add(-train_resp_mean)
            train_scale.response[i]:div(train_resp_std)
            end
        for i=1,val_scale:size() do
            val_scale.response[i]:add(-train_resp_mean)
            val_scale.response[i]:div(train_resp_std)
            end
        for i=1,test_scale:size() do
            test_scale.response[i]:add(-train_resp_mean)
            test_scale.response[i]:div(train_resp_std)
            end
        out.response_scalings = {train_resp_mean,train_resp_std}
        end

    out.datasets = {train_scale,val_scale,test_scale}
    return out
end


function torch_model_create(architecture
,model_type
        , use_l1,use_dropout, l1_val, dropout_val
    )

    local torch_model = nn.Sequential()
    if model_type == "conv" then
        local filter_width = architecture[1]
        local filter_height = architecture[2]
        local out_channels = architecture[3]
        local width_out = peptide_length - filter_width + 1
        local height_out = 20 - filter_height + 1
        architecture[3] = out_channels * width_out * height_out
        torch_model:add(nn.SpatialConvolution(1,out_channels
                    ,filter_width,filter_height))
        torch_model:add(nn.View(out_channels * width_out * height_out))
        torch_model:add(nn.Tanh())
        if use_l1 == "True" then
            torch_model:add(nn.L1Penalty(l1_val))
            end
        if use_dropout == "True" then
            torch_model:add(nn.Dropout(dropout_val))
            end
        if #architecture > 3 then
            for i = 3,(#architecture - 1) do
                torch_model:add(nn.Linear(architecture[i],architecture[i + 1]))
                torch_model:add(nn.Tanh())
                if use_l1 == "True" then
                    torch_model:add(nn.L1Penalty(l1_val))
                    end
                if use_dropout == "True" then
                    torch_model:add(nn.Dropout(dropout_val))
                    end
                end
            end
        end
    if model_type == "mlp" then
            for i = 1,(#architecture - 1) do
            torch_model:add(nn.Linear(architecture[i],architecture[i + 1]))
            torch_model:add(nn.Tanh())
            if use_l1 == "True" then
                torch_model:add(nn.L1Penalty(l1_val))
                end
            if use_dropout == "True" then
                torch_model:add(nn.Dropout(dropout_val))
                end
            end
        end
    if model_type == "lookup" then
      feature_length = architecture[1]
      conv_num = architecture[2]
      window_num = architecture[3]
      torch_model = nn.Sequential()
      torch_model:add(nn.LookupTable(20,feature_length))
      torch_model:add(nn.TemporalConvolution(feature_length
          ,conv_num
          ,peptide_length + 1 - window_num,1))
      torch_model:add(nn.View())
      torch_model:add(nn.Tanh())
      torch_model:add(nn.Linear(conv_num * window_num,architecture[4]))
      torch_model:add(nn.Tanh())
      if use_l1 == "True" then
        torch_model:add(nn.L1Penalty(l1_val))
        end
      if use_dropout == "True" then
        torch_model:add(nn.Dropout(dropout_val))
        end
      for i = 4,#architecture - 1 do
        torch_model:add(nn.Linear(architecture[i],architecture[i + 1]))
        torch_model:add(nn.Tanh())
        if use_l1 == "True" then
          torch_model:add(nn.L1Penalty(l1_val))
          end
        if use_dropout == "True" then
          torch_model:add(nn.Dropout(dropout_val))
          end
      end
    end

    torch_model:add(nn.Linear(architecture[#architecture],1))
    return torch_model
end

function model_cv(raw_data,number
        ,architecture, model_type,use_l1,use_dropout, l1_val, dropout_val
        , learning_rate,max_iters
    ,data_type,cuda)

    local results = {}
    for i = 1,number do

       local trainset, valset, testset, indices = torch_process(raw_data,resp_col
            ,cuda,feature_end,.8,.19,.01)

        feature_scale = "No"
       if data_type == "blosum" then
            feature_scale = "Yes"
            end

       local scale_set = scale_data(trainset,valset,testset,feature_scale,"Yes")

        print(data_type .. scale_set.datasets[1].response[1][1])


        --Convert the 2D data into 4D tensors for convolution
        if model_type == "conv" then
            scale_set.datasets[1].data = torch.reshape(scale_set.datasets[1].data
                ,scale_set.datasets[1].data:size()[1],1,peptide_length,20)
            scale_set.datasets[1].data = scale_set.datasets[1].data:transpose(4,3)

            scale_set.datasets[2].data = torch.reshape(scale_set.datasets[2].data
                ,scale_set.datasets[2].data:size()[1],1,peptide_length,20)
            scale_set.datasets[2].data = scale_set.datasets[2].data:transpose(4,3)

            scale_set.datasets[3].data = torch.reshape(scale_set.datasets[3].data
                ,scale_set.datasets[3].data:size()[1],1,peptide_length,20)
            scale_set.datasets[3].data = scale_set.datasets[3].data:transpose(4,3)
            end

        --Create the model object
        local torch_model = torch_model_create(architecture
            ,model_type,use_l1,use_dropout, l1_val, dropout_val)
        local criterion = nn.MSECriterion()

print("test post model")

        if cuda == "True" then
            criterion = criterion:cuda()
            torch_model = torch_model:cuda()
            end

        local trainer = nn.StochasticGradient(torch_model, criterion)
        trainer.learningRate = learning_rate
        trainer.maxIteration = max_iters


        trainer:train(scale_set.datasets[1])

        results[i] = generate_results(torch_model,scale_set.datasets[2]
        ,indices.val_seq,scale_set.response_scalings
		,"True","True","True","True")
	print(results[i])

        end
    return results
end

function generate_results(model,data,indices,response_scalings
,pearson_all
        ,spearman_all,spearman_tops,spearman_toppreds)

    local new_preds = predict_model(model,data)
    local out = {}
    local trend_cutoff = (.5 - response_scalings[1]) / response_scalings[2]

    if pearson_all == "True" then
        out.pearson_all = pearson(new_preds.predicted,new_preds.true_val)
    end
    if spearman_all == "True" then
        local spearman_pairs = {}
        for i=1,data:size() do
            spearman_pairs[i] = {new_preds.predicted[i],new_preds.true_val[i]}
        end
        out.spearman_all = math.spearman(spearman_pairs)
    end
    if spearman_tops == "True" then
        local cutoff = torch.sort(new_preds.true_val,1,true)
        cutoff = cutoff[torch.min(torch.Tensor({
          101
          ,data:size()
        }))]
        cutoff = torch.max(torch.Tensor({cutoff,trend_cutoff}))
        local spearman_pairs_tops = {}
        local iter = 1
        for i=1,data:size() do
            if new_preds.true_val[i] > cutoff then
                if tcr_data[indices[i]][feature_end + 4] + 0 > 0 then
                spearman_pairs_tops[iter] = {new_preds.predicted[i],new_preds.true_val[i]}
                iter = iter + 1
                end
            end
        end
        print(#spearman_pairs_tops)
        --out.top_pairs = spearman_pairs_tops
        out.spearman_tops = math.spearman(spearman_pairs_tops)
    end
    if spearman_toppreds == "True" then
        local cutoff = torch.sort(new_preds.predicted,1,true)
        cutoff = cutoff[torch.min(torch.Tensor({
          101
          ,data:size()
        }))]
        local spearman_pairs_toppreds = {}
        local iter = 1
        for i=1,data:size() do
          if new_preds.predicted[i] > cutoff then
                if tcr_data[indices[i]][feature_end + 4] + 0 > 0 then
                    if new_preds.true_val[i] > trend_cutoff then
                        spearman_pairs_toppreds[iter] = {new_preds.predicted[i],new_preds.true_val[i]}
                        iter = iter + 1
                    end
                end
            end
        end
        print(#spearman_pairs_toppreds)
        out.spearman_toppreds = math.spearman(spearman_pairs_toppreds)
    end
    return out
end
