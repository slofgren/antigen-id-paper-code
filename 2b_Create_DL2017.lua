require 'nn'
require 'torch_dl_support'
require 'csvigo'

--Key parameters

cmd = torch.CmdLine()
cmd:option('-rawdata_path',"")
cmd:option('-tcr_name',"tcr9")
cmd:option('-peptide_length',8)
cmd:option('-out_path',"")
cmd:option('-prot_name','Human')
cmd:option('-chunks',"")

params = cmd:parse(arg)
print(params)

tcr_name = params.tcr_name
peptide_length = params.peptide_length
rawdata_path = params.rawdata_path
out_path = params.out_path
prot_name = params.prot_name
chunks = params.chunks

----Example values
--rawdata_path = "/Users/shanelofgren/Downloads"
--tcr_name = "tcr81s"
--peptide_length = 9
--out_path = "/Users/shanelofgren/Downloads"
--data_type = "onehot"
--prot_name = "Human"
--chunks = 15

data_type = "onehot"
feature_end = peptide_length * 20
resp_col = feature_end + 6

tcr_data = csvigo.load({path = rawdata_path .. "/" .. tcr_name .. "_onehot.csv"
        ,separator = ",",mode = "raw"})


trainset, valset, testset,indices = torch_process(tcr_data,resp_col,"False"
    ,feature_end
    ,.8,.01,.19)


tcr_data = nil
collectgarbage("collect")
collectgarbage("count")

scale_set = scale_data(trainset,valset,testset,"No","Yes")

trainset = nil
valset = nil
testset = nil
collectgarbage("collect")
collectgarbage("count")

---Initialize model
torch_model = torch_model_create({feature_end,10,5},"mlp","False","False",0,0,"False")

criterion = nn.MSECriterion()

--train
trainer = nn.StochasticGradient(torch_model, criterion)
trainer.learningRate = 0.0005
trainer.maxIteration = 10
trainer:train(scale_set.datasets[1])


trainer.learningRate = 0.000125
trainer.maxIteration = 10
trainer:train(scale_set.datasets[1])


---Write predictions for each of the datasets
export_results(scale_set,indices
    ,out_path
,torch_model)


scale_set.datasets = nil
collectgarbage("collect")
collectgarbage("count")

--Export weights for those who want to preserve model / score sequences by hand
for i=1,#torch_model.modules do
  if torch_model.modules[i].weight ~= nil then
        out_path_wgts = out_path .. "/" .. tcr_name .. "_wgts_" .. i .. ".csv"
        out_wgts = torch.totable(torch_model.modules[i].weight)
        csvigo.save{path = out_path_wgts,data = out_wgts,separator = ",",mode = "raw"}
        end
     if torch_model.modules[i].bias ~= nil then
        out_path_bias = out_path .. "/" .. tcr_name .. "_bias_" .. i .. ".csv"
        out_bias = torch.totable(torch_model.modules[i].bias)
        for j = 1,#out_bias do
            out_bias[j] = {out_bias[j],0}
            end
        csvigo.save{path = out_path_bias,data = out_bias,separator = ",",mode = "raw"}
        end
    end


trainset = nil
valset = nil
testset = nil
scale_set.datasets = nil
tcr_data = nil
collectgarbage("collect")
collectgarbage("count")


for i = 1,chunks do
        collectgarbage("collect")
        collectgarbage("count")
    prt_chunk = torch.Tensor(
        csvigo.load({path = rawdata_path ..  "/" .. prot_name .. "_proteome_onehot_" .. i .. ".csv"
        ,separator = ",",mode = "raw"})
        )
        collectgarbage("collect")
        collectgarbage("count")
      preds = torch_model:forward(prt_chunk[{{},{1,feature_end}}])
      out_table = torch.Tensor((#prt_chunk)[1],2)
      out_table[{{},{1}}] = prt_chunk[{{},{(#prt_chunk)[2]}}]
      out_table[{{},{2}}] = preds
      out_table[{{},{2}}] =     out_table[{{},{2}}]:add(scale_set.response_scalings[1])
      out_table[{{},{2}}] =     out_table[{{},{2}}]:mul(scale_set.response_scalings[2])
      prt_chunk = nil
      collectgarbage("collect")
      collectgarbage("count")
      out_table = torch.totable(out_table)
      collectgarbage("collect")
      collectgarbage("count")
      csvigo.save{path = out_path ..  "/" .. prot_name .. "_" .. tcr_name .. "_DL_scored_" .. i .. ".csv"
      ,data = out_table,separator = ",",mode = "raw"}
    out_table = nil
    out_table_temp = nil
    preds = nil
    prt_chunk = nil
    collectgarbage("collect")
    collectgarbage("count")
    end
